﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameControllerScript : MonoBehaviour {

    public GameObject NextTilePosition;
    public GameObject Farm;
    public GameObject City;
    public GameObject Water;
    public GameObject WasteLand;
    public GameObject[] BlankTiles;
    public List<blankTileBehaviour> _blankTileScripts;
    public Text ScoreText;
    float _currentScore;
    float _displayScore;
    public Text ScoreLastTurnText;
    float _scoreLastTurn;
    TurnsIndicatorScript _turnsIndicator;
    GameObject _infoPanel;

    public AudioSource GameOverAudio;

    GameObject _nextTile; //reference to the last tile generated
    List<GameObject> _tileBag; //place to store our next bag of pieces

    int _tileAnimationWaitSemaphore; //semaphore used to wait until tiles are finished animating.
	object _animationSemaphoreLock = new object();

    int _scoreParticleWaitSemaphore; //semaphore used to wait until the score particles are finished animating.
    object _scoreParticleSemaphoreLock = new object();

    // Use this for initialization
    void Start () {
        GameOverAudio.Play();
        _currentScore = 0;
        _tileAnimationWaitSemaphore = 0;
        _infoPanel = GameObject.Find("InfoBox");
        BlankTiles = GameObject.FindGameObjectsWithTag("BlankTile");
        foreach (GameObject tile in BlankTiles)
        {
            tile.GetComponent<blankTileBehaviour>().SetNeighbours(BlankTiles);
            _blankTileScripts.Add(tile.GetComponent<blankTileBehaviour>());
        }
        _turnsIndicator = GameObject.Find("TurnIndicators").GetComponent<TurnsIndicatorScript>();
        _turnsIndicator.SetTurns(30);
        PresentNextTile(true);
    }
	
	// Update is called once per frame
	void Update () {
        //value of zero means the player is still deciding where to place,
        //value of more than 1 means we're waiting for tile animations to finish,
        //value of exactly 1 means a tile has been placed and animations are done.
		lock(_animationSemaphoreLock)
		{
			if (_tileAnimationWaitSemaphore == 1)
			{
				_tileAnimationWaitSemaphore--;
				TurnComplete();
			}
		}

        //value of zero means nothing is happening - no particles are flying around,
        //value of more than 1 means we're waiting for score particle animations to finish,
        //value of exactly 1 means particles are finished, and we should update the score
        lock (_scoreParticleSemaphoreLock)
        {
            if (_scoreParticleWaitSemaphore == 1)
            {
                _scoreParticleWaitSemaphore--;
                ScoreText.text = "Score: " + _currentScore;
                _displayScore = _currentScore;
                ScoreLastTurnText.text = "(Last turn: " + _scoreLastTurn + ")";
            }
        }
    }

    public void TurnComplete()
    {
        _turnsIndicator.RemoveTurn();

        _scoreLastTurn = CalculateScore();
        _currentScore += _scoreLastTurn;
        
        if (_turnsIndicator.GetNumTurns() <= 0)
        {
            StartCoroutine(ShowGameOver());
        }
        else
        {
            PresentNextTile();
        }
    }

    IEnumerator ShowGameOver()
    {
        yield return new WaitForSeconds(1.5f);
        GameOverAudio.Play();
        _infoPanel.SetActive(false);
        gameObject.SendMessage("ShowScoreController", _currentScore);
    }

    //randomly selects next tile and plops it on the screen for player to select.
    private void PresentNextTile(bool firstTime = false)
    {
        //use the tetris algorithm
        if(_tileBag == null || _tileBag.Count == 0)
        {
            List<GameObject> tempTileBag = new List<GameObject>();
            //no farms allowed first time, it's annoying
            tempTileBag.Add(Farm);
            tempTileBag.Add(City);
            tempTileBag.Add(Water);
            tempTileBag.Add(WasteLand);
            _tileBag = new List<GameObject>();
            while(tempTileBag.Count > 0)
            {
                int randomIndex = (int)Random.Range(0.0f, (float)tempTileBag.Count - float.Epsilon);
                _tileBag.Add(tempTileBag[randomIndex]);
                tempTileBag.RemoveAt(randomIndex);
            }
        }

        int index = 0;
        //not allowed farm before water
        if (firstTime)
        {
            int farmIndex = _tileBag.FindIndex(e => e == Farm);
            int waterIndex = _tileBag.FindIndex(e => e == Water);
            if (farmIndex < waterIndex)
            {
                _tileBag[farmIndex] = Water;
                _tileBag[waterIndex] = Farm;
            }
        }
        InstantiateTile(_tileBag[index]);
        _tileBag.RemoveAt(index);
    }

    void InstantiateTile(GameObject tile)
    {
        _nextTile = Instantiate(tile, NextTilePosition.transform.position,
                                    Quaternion.Euler(new Vector3(270, 0, 0))) as GameObject;
        TileBehaviour script = _nextTile.GetComponent<TileBehaviour>();

        script.Decorate();

        float degreesRandomness = 60.0f;
        Vector3 randomizeRot = new Vector3(Random.Range(-degreesRandomness / 2, degreesRandomness / 2),
                                            Random.Range(-degreesRandomness / 2, degreesRandomness / 2),
                                            Random.Range(-degreesRandomness / 2, degreesRandomness / 2));
        _nextTile.transform.Rotate(randomizeRot);

        _nextTile.GetComponent<TileBehaviour>().SetPhysics(true);
        _nextTile.GetComponent<Rigidbody>().velocity = new Vector3(0, 1, 0); //maybe play with this
        _nextTile.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(-1.0f, 1.0f),
                                                                            Random.Range(-1.0f, 1.0f),
                                                                            Random.Range(-1.0f, 1.0f));
    }

    //called by tile when it's been legally placed.
    public void TileDropped()
    {
		lock(_animationSemaphoreLock)
		{
			_tileAnimationWaitSemaphore++; //nonzero value on this guy means we've dropped a tile.
		}
        UpdateTiles();
    }

    private void UpdateTiles()
    {
        //First do farms, so that if any of them turn into wastelands we can do that first
        UpdateFarms();
        //then do cities, their leveling/warring has to be sorted out
        UpdateCities();

        //the rest happens when we're happy all update animations are completed, which is checked for in Update();
    }

    float CalculateScore()
    {
        lock (_scoreParticleSemaphoreLock)
        {
            _scoreParticleWaitSemaphore = 1; //reset semaphore, ignore previous particles just wait for this new batch to finish.
        }
        float score = 0;
        score += ScoreCities();
        score += ScoreFarms();
        score += ScoreWater();
        score += ScoreWastelands();
        return score;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); // loads current scene
    }

    public void ReportTileAnimationComplete()
    {
		lock(_animationSemaphoreLock)
		{
			_tileAnimationWaitSemaphore--;
		}
    }

    public void ReportScoreParticleAnimationComplete()
    {
        lock (_scoreParticleSemaphoreLock)
        {
            _scoreParticleWaitSemaphore--;
            if (_scoreParticleWaitSemaphore > 2)
            {
                //don't update on last one, otherwise the score might have to be adjusted down,
                //because this particle could represent less than 10 points.
                _displayScore += 10;
                ScoreText.text = "Score: " + _displayScore;
            }
        }
    }

    /// ///////////////////////////////////////////////////////UPDATE/SCORING LOGIC///////////////////////////////////////////

    //turns the tile placed on blankTile to wasteland.
    void TurnPlacedTileToWasteland(GameObject blankTile)
    {
        blankTileBehaviour blankTileScript = blankTile.GetComponent<blankTileBehaviour>();
        GameObject currentPlacedTile = blankTileScript.CurrentTile;
        if(currentPlacedTile != null)
        {
            TileBehaviour placedTileScript = currentPlacedTile.GetComponent<TileBehaviour>();
            placedTileScript.TurnToWasteland(2.0f);
			lock(_animationSemaphoreLock)
			{
				_tileAnimationWaitSemaphore++;
			}
        }
    }

    void SetTileScore(blankTileBehaviour tile, float score)
    {
        tile.SetScore(score);
        if (score > 0)
        {
            //1 for every 10 points or part thereof
            int particleSize = ((int)score) / 10;
            if ((int)score % 10 > 0)
            {
                particleSize++;
            }

            lock (_scoreParticleSemaphoreLock)
            {
                _scoreParticleWaitSemaphore += 1;
            }
            StartCoroutine(tile.SpawnScoreParticles(particleSize));
        }
    }

    /// ///////////////CITIES//////////////////////////////////////////

    /*
    city - 10 points* size
    size increases every turn
    
    size max = 1 + number of joined contiguous farm areas
                + 1 if neighbouring water
    
    if 2 or more neighbouring, WAR, freak out and kill any neighbours that are smalle than you. */
    private void UpdateCities()
    {
        //first do level up calcs
        LevelUpCities();

        //then get all cities and rank them by size, so that we can resolve wars.
        List<blankTileBehaviour> cities = GetAllCitiesInLevelOrder();

        //now we have a list of cities in size order - resolve their wars
        ResolveCityWars(ref cities);
    }

    //levels up cities
    void LevelUpCities()
    {
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            if (currentBlankTile.GetCurrentPlacedType() == TileBehaviour.eTileType.City)
            {
                //what is the maximum level for this city?--------------------------
                //1 + number of connected farms
                //+ 1 if touching water
                int maxLevel = 1; //baseline
                                  //+1 for each connected farm
                List<blankTileBehaviour> contiguousFarms = new List<blankTileBehaviour>();
                foreach (blankTileBehaviour neighboursScript in currentBlankTile.NearestNeighbours)
                {
                    if (neighboursScript.GetCurrentPlacedType() == TileBehaviour.eTileType.Farm &&
                        !contiguousFarms.Contains(neighboursScript))
                    {
                        contiguousFarms.AddRange(neighboursScript.GetContiguousTiles());
                    }
                }
                maxLevel += contiguousFarms.Count;
                //+1 if touching water
                if (currentBlankTile.GetNumNeighboursOfType(1, TileBehaviour.eTileType.Water) > 0)
                {
                    maxLevel++;
                }

                //level up .------------------------------------------------------------
                GameObject currentPlacedTile = currentBlankTile.CurrentTile;
                CityScript cityScript = currentPlacedTile.GetComponent<CityScript>();
                if (cityScript.Level < maxLevel)
                {
                    cityScript.Level++;
                }
                else if (cityScript.Level > maxLevel)
                {
                    cityScript.Level = maxLevel;
                }
            }
        }
    }

    List<blankTileBehaviour> GetAllCitiesInLevelOrder()
    {
        List<blankTileBehaviour> cities = new List<blankTileBehaviour>();
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            if (currentBlankTile.GetCurrentPlacedType() == TileBehaviour.eTileType.City)
            {
                //insert into correct place in list.
                int thisOnesLevel = currentBlankTile.CurrentTile.GetComponent<CityScript>().Level;
                bool foundASpot = false;
                foreach (blankTileBehaviour city in cities)
                {
                    int oneInTheListsLevel = city.CurrentTile.GetComponent<CityScript>().Level;
                    if (thisOnesLevel > oneInTheListsLevel)
                    {
                        cities.Insert(cities.IndexOf(city), currentBlankTile);
                        foundASpot = true;
                        break;
                    }
                }
                if (!foundASpot)
                {
                    cities.Add(currentBlankTile);
                }
            }
        }
        return cities;
    }

    //resolves wars between cities.  Modifies the given list so some could be wasteland when this is done.
    void ResolveCityWars(ref List<blankTileBehaviour> cities)
    {
        foreach (blankTileBehaviour warringCity in cities)
        {
            ////resolve wars------------------------------------------------------
            if (warringCity.GetCurrentPlacedType() == TileBehaviour.eTileType.City) //could already be destroyed
            {
                List<blankTileBehaviour> neighbouringCitiesToDealWith = warringCity.GetNeighboursOfType(TileBehaviour.eTileType.City);
                //if you have more than 1 neighbour, destroy anyone smaller than you.
                if (neighbouringCitiesToDealWith.Count > 1)
                {
                    int myLevel = warringCity.CurrentTile.GetComponent<CityScript>().Level;
                    foreach (blankTileBehaviour enemy in neighbouringCitiesToDealWith)
                    {
                        //fight!
                        int opponentLevel = enemy.CurrentTile.GetComponent<CityScript>().Level;
                        if (opponentLevel < myLevel)
                        {
                            //destroy opponent
                            TurnPlacedTileToWasteland(enemy.gameObject);
                        }
                    }
                }
            }
        }
    }

    private float ScoreCities()
    {
        float totaCityScores = 0;
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            if (currentBlankTile.GetCurrentPlacedType() == TileBehaviour.eTileType.City)
            {
                GameObject currentPlacedTile = currentBlankTile.CurrentTile;
                CityScript cityScript = currentPlacedTile.GetComponent<CityScript>();
                float cityScore = cityScript.Level * 10.0f;

                //wasteland effect
                var neighbouringWastelands = currentBlankTile.GetNeighboursOfType(TileBehaviour.eTileType.WasteLand);
                foreach (var wl in neighbouringWastelands)
                {
                    cityScore *= 0.9f;
                }
                cityScript.MarkWastelandEffect(neighbouringWastelands.Count);
                cityScore = Mathf.Round(cityScore);

                SetTileScore(currentBlankTile, cityScore);

                totaCityScores += cityScore;
            }
        }
        return totaCityScores;
    }

    /// ///////////////FARMS/////////////////////////////////////////////////
    void UpdateFarms()
    {
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            GameObject currentPlacedTile = currentBlankTile.CurrentTile;
            if (currentPlacedTile != null)
            {
                TileBehaviour currentPlacedTileScript = currentPlacedTile.GetComponent<TileBehaviour>();
                if (currentPlacedTileScript.GetTileType() == TileBehaviour.eTileType.Farm)
                {
                    //if no water within 2 tiles, turn to wasteland
                    if (currentBlankTile.GetNumNeighboursOfType(2, TileBehaviour.eTileType.Water) < 1)
                    {
                        TurnPlacedTileToWasteland(currentBlankTile.gameObject);
                        continue;
                    }
                }
            }
        }
    }

    private float ScoreFarms()
    {
        float totaFarmScores = 0;
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            if (currentBlankTile.GetCurrentPlacedType() == TileBehaviour.eTileType.Farm)
            {
                //farms are 30 points minus 10 for every neighbouring farm, minimum of 0
                int numNeighbouringFarms = currentBlankTile.GetNumNeighboursOfType(1, TileBehaviour.eTileType.Farm);
                float provisionalScore = 30 - (numNeighbouringFarms * 10);
                float actualScore = provisionalScore > 0 ? provisionalScore : 0;

                //wasteland effect
                var neighbouringWastelands = currentBlankTile.GetNeighboursOfType(TileBehaviour.eTileType.WasteLand);
                foreach (var wl in neighbouringWastelands)
                {
                    actualScore *= 0.9f;
                }
                currentBlankTile.CurrentTile.GetComponent<FarmScript>().MarkWastelandEffect(neighbouringWastelands.Count);
                actualScore = Mathf.Round(actualScore);

                SetTileScore(currentBlankTile, actualScore);

                totaFarmScores += actualScore;
            }
        }
        return totaFarmScores;
    }

    /// ///////////////WATER/////////////////////////////////////////////////

    private float ScoreWater()
    {
        float totaWaterScores = 0;
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            if (currentBlankTile.GetCurrentPlacedType() == TileBehaviour.eTileType.Water)
            {
                //water - points = number of contiguous*4 - num neighbouring cities*their sizes
                int numtouching = currentBlankTile.GetContiguousTiles().Count;
                float waterScore = numtouching * 5;
                foreach (blankTileBehaviour neighbourCity in
                            currentBlankTile.GetNeighboursOfType(TileBehaviour.eTileType.City))
                {
                    waterScore -= neighbourCity.CurrentTile.GetComponent<CityScript>().Level;
                }

                //wasteland effect
                var neighbouringWastelands = currentBlankTile.GetNeighboursOfType(TileBehaviour.eTileType.WasteLand);
                foreach (var wl in neighbouringWastelands)
                {
                    waterScore *= 0.9f;
                }
                currentBlankTile.CurrentTile.GetComponent<WaterScript>().MarkWastelandEffect(neighbouringWastelands.Count);

                waterScore = Mathf.Round(waterScore);
                SetTileScore(currentBlankTile, waterScore);
                totaWaterScores += waterScore;
            }
        }
        return totaWaterScores;
    }

    /// ///////////////WASTELAND/////////////////////////////////////////////////

    private float ScoreWastelands()
    {
        float totaWastelandScores = 0;
        foreach (blankTileBehaviour currentBlankTile in _blankTileScripts)
        {
            if (currentBlankTile.GetCurrentPlacedType() == TileBehaviour.eTileType.WasteLand)
            {
                //wasteland - -5 points
                //neighbouring tiles -10%
                //if tile surrounded by 4 or more wastelands, becomes wasteland
                //if 3 wastelands touching, they're not so bad, maybe 0 or +5?
                SetTileScore(currentBlankTile, -5);
                totaWastelandScores -= 5;
            }
        }
        return totaWastelandScores;
    }
}
