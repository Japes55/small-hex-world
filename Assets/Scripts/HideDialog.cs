﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideDialog : MonoBehaviour {

    public GameObject ThingToHide;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void HideTheDialog()
    {
        ThingToHide.SetActive(!ThingToHide.activeSelf);
    }
}
