﻿using UnityEngine;
using System.Collections;

public class SetupUI : MonoBehaviour {


    Camera _mainCamera;
    GameObject _scorePos;
    GameObject _scoreText;
    GameObject _scoreLastTurnPos;
    GameObject _scoreLastTurnText;
    GameObject _placeNextPos;
    GameObject _placeNextText;
    GameObject _turnsPos;
    GameObject _turnsText;
    GameObject _infoBoxPos;
    GameObject _infoBoxPanel;

    // Use this for initialization
    void Start () {
        _mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

        _scorePos = GameObject.Find("Score Position");
        _scoreText = GameObject.Find("Score Indicator");

        _scoreLastTurnPos = GameObject.Find("Score Last Turn Position");
        _scoreLastTurnText = GameObject.Find("Score Last Turn Indicator");

        _placeNextPos = GameObject.Find("PlaceNext Position");
        _placeNextText = GameObject.Find("PlaceNext Indicator");

        _turnsPos = GameObject.Find("RemainingTurns Position");
        _turnsText = GameObject.Find("RemainingTurns Indicator");

        _infoBoxPos = GameObject.Find("InfoBox Position");
        _infoBoxPanel = GameObject.Find("InfoBox");
    }

    // Update is called once per frame
    void Update () {

        //lock the UI elements to a position in world space
        Vector3 scoreScreenPos = _mainCamera.WorldToScreenPoint(_scorePos.transform.position);
        _scoreText.transform.position = scoreScreenPos;

        Vector3 scoreLastTurnScreenPos = _mainCamera.WorldToScreenPoint(_scoreLastTurnPos.transform.position);
        _scoreLastTurnText.transform.position = scoreLastTurnScreenPos;

        Vector3 placeNextScreenPos = _mainCamera.WorldToScreenPoint(_placeNextPos.transform.position);
        _placeNextText.transform.position = placeNextScreenPos;

        if (_turnsPos != null && _turnsText != null)
        {
            Vector3 remainingTurnsScreenPos = _mainCamera.WorldToScreenPoint(_turnsPos.transform.position);
            _turnsText.transform.position = remainingTurnsScreenPos;
        }

        Vector3 infoBoxScreenPos = _mainCamera.WorldToScreenPoint(_infoBoxPos.transform.position);
        _infoBoxPanel.transform.position = infoBoxScreenPos;
    }
    
}
