﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnsIndicatorScript : MonoBehaviour {

    public GameObject Indicator;
    List<GameObject> _turnIndicators;

    public Text TurnsText;
    TurnsIndicatorScript _turnsIndicator;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetTurns(int turns)
    {
        SetList();
        float xOffset = 0;
        float zOffset = 0;
        for (int i = 0; i < turns; i++)
        {
            if(i != 0 && i%15 == 0)
            {
                xOffset = 0.0f;
                zOffset += -0.8f;
            }

            GameObject turnIndicator = Instantiate(Indicator,
                    transform.position + new Vector3(xOffset, 0.0f, zOffset),
                    Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
            turnIndicator.transform.SetParent(transform);
            _turnIndicators.Add(turnIndicator);
            xOffset += 0.15f;
        }

        UpdateText();
    }

    public void RemoveTurn()
    {
        SetList();
        if (_turnIndicators.Count > 0)
        {
            GameObject lastObj = _turnIndicators[_turnIndicators.Count - 1];
            _turnIndicators.Remove(lastObj);
            Destroy(lastObj);
        }
        UpdateText();
    }

    void UpdateText()
    {
        TurnsText.text = "Turns Left:\n";// + _turnIndicators.Count;
    }

    public int GetNumTurns()
    {
        SetList();
        return _turnIndicators.Count;
    }

    void SetList()
    {
        if (_turnIndicators == null)
        {
            _turnIndicators = new List<GameObject>();
        }
    }
}
