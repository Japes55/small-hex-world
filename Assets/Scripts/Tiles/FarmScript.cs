﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmScript : TileBehaviour {

	// Use this for initialization
	void Start () {
        base.DoGeneralStart();
    }

    // Update is called once per frame
    void Update () {
        BaseUpdate();
    }

    public override void Decorate()
    {
    }

    protected override string GetName()
    {
        return "FARM";
    }

    protected override string GetInfo()
    {
        return
@"Farms generate 30 points each, but lose 10 points for each neighbouring farm.

They need water within 2 tiles, otherwise they turn into wasteland.";
    }

    public override Color GetSpecificHighLightColour(blankTileBehaviour placementTile = null)
    {
        //if no water within 2 tiles, turn to wasteland
        if (placementTile == null || 
            placementTile.GetNumNeighboursOfType(2, TileBehaviour.eTileType.Water) > 0)
        {
            return Color.green;
        }
        else
        {
            return Color.red;
        }
    }

    public override eTileType GetTileType()
    {
		//We do things this way to simplify the logic in GameControllerScript
		if(IsBusyTurningToWasteland())
		{
			return eTileType.WasteLand;
		}
		else
		{
			return eTileType.Farm;
		}
    }
}
