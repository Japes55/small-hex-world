﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScript : TileBehaviour
{

	// Use this for initialization
	void Start () {
        base.DoGeneralStart();
    }

    // Update is called once per frame
    void Update () {
        BaseUpdate();
    }

    public override void Decorate()
    {
    }

    protected override string GetName()
    {
        return "WATER";
    }

    protected override string GetInfo()
    {
        return
@"Water tiles generate 5 points for every water tile they are connected to.

Their production is reduced by the level of any bordering city.";
    }

    public override Color GetSpecificHighLightColour(blankTileBehaviour placementTile = null)
    {
        return Color.blue;
    }

    public override eTileType GetTileType()
    {
        //We do things this way to simplify the logic in GameControllerScript
        if (IsBusyTurningToWasteland())
        {
            return eTileType.WasteLand;
        }
        else
        {
            return eTileType.Water;
        }
    }
}
