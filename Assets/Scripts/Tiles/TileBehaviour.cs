﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//basic behaviour for all tiles
public class TileBehaviour : MonoBehaviour {

    GameControllerScript _theGameController;
    GameObject _tilePlacementPosition;
    public bool IsDraggable;
    public GameObject WasteLand;

    Vector3 _dist;
    float _posX;
    float _posY;
    float _wastelandTransformationTime; //serves as both a flag indicating whether or not we should fade, and a time in seconds to fade.
    float _wastelandTransformationStart;

    GameObject _currentTargetDrop;

    Text _infoPanelHeading;
    Text _infoPanelText;

    public AudioSource DropSoundAudio;
    public AudioClip[] clips;

    public GameObject[] Props;

    Color _startingColor;

    public enum eTileType
    {
        Blank,
        Farm,
        City,
        Water,
        WasteLand
    }

    // Use this for initialization
    void Start()
    {
        DoGeneralStart();
    }

    protected void DoGeneralStart () {
        _theGameController = GameObject.Find("GameController").GetComponent<GameControllerScript>();
        _tilePlacementPosition = GameObject.Find("NewTilePosition");
        GameObject infoPanel = GameObject.Find("InfoBox");
        _infoPanelHeading = infoPanel.transform.Find("Heading").GetComponent<Text>();
        _infoPanelText = infoPanel.transform.Find("Body").GetComponent<Text>();
        _wastelandTransformationTime = 0.0f;
        _startingColor = GetComponent<Renderer>().material.color;
        SetDefaultInfoText(false);
    }

    // BaseUpdate is called by children in their Update()
    protected void BaseUpdate () {
        if (IsBusyTurningToWasteland())
        {
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer renderer in renderers)
            {
                var material = renderer.material;
                var color = material.color;

                material.color = new Color(color.r, color.g, color.b, 1 - ((Time.time - _wastelandTransformationStart) / _wastelandTransformationTime));

                if (material.color.a <= 0.0f)
                {
                    _wastelandTransformationTime = 0.0f;
                    GameObject wasteLand = Instantiate(WasteLand, transform.position,
                                            Quaternion.Euler(new Vector3(270, 0, 0))) as GameObject;
                    _theGameController.ReportTileAnimationComplete();
                    //this drops the wasteland on our current tile, causing us to be destroyed.
                    wasteLand.GetComponent<TileBehaviour>().DropOnBlankTile(_currentTargetDrop);
                    break;
                }
            }
        }
    }
	
	public bool IsBusyTurningToWasteland()
	{
		return (_wastelandTransformationTime > 0.0f);
	}

    void OnMouseDown()
    {
        if (IsDraggable)
        {
            _dist = Camera.main.WorldToScreenPoint(transform.position); //my screen point at start
            _posX = Input.mousePosition.x - _dist.x; //offset from mouse to my pos
            _posY = Input.mousePosition.y - _dist.y; //offset from mouse to my pos
            SetPhysics(false);
        }
    }

    //handle mouseover info panel
    void OnMouseEnter()
    {
        FadeToText(_infoPanelHeading, GetName());
        FadeToText(_infoPanelText, GetInfo());
    }

    //sets the text of a Text, with a nice fade.
    public void FadeToText(Text text, string msg)
    {
        StartCoroutine(FadeText(text, false));
        text.text = msg;
        StartCoroutine(FadeText(text, true));
    }

    //fades text in/out over time.
    //text: the unity Text object to set
    //fadein: is this a fade IN, or a fade OUT?
    private IEnumerator FadeText(Text text, bool fadeIn)
    {
        float duration = 0.15f;
        float currentTime = 0f;
        float startAlpha = fadeIn ? 0.0f : 1.0f;
        float endAlpha = fadeIn ? 1.0f : 0.0f;
        while (currentTime < duration)
        {
            float alpha = Mathf.Lerp(startAlpha, endAlpha, currentTime / duration);
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
            currentTime += Time.deltaTime;
            yield return null;
        }
        yield break;
    }

    void OnMouseExit()
    {
        SetDefaultInfoText(true);
    }

    void SetDefaultInfoText(bool fade)
    {
        string heading = "Welcome To A Small Hex World!";
        string body =
@"The aim of the game? Score as many points as possible with a limited number of turns.

Place tiles by dragging them on to the board.  Mouseover tiles for more information.

Good luck!";

        if (fade)
        {
            FadeToText(_infoPanelHeading, heading);
            FadeToText(_infoPanelText, body);
        }
        else
        {
            _infoPanelHeading.text = heading;
            _infoPanelText.text = body;
        }
    }

    void OnMouseDrag()
    {
        if (IsDraggable)
        {
            //get screen pos i want to be at
            float xpos = Input.mousePosition.x - _posX;
            float ypos = Input.mousePosition.y - _posY;
            float zpos = _dist.z;
            if(xpos < 0) { xpos = 0; }
            if(ypos < 0) { ypos = 0; }
            if(xpos > Screen.width) { xpos = Screen.width; }
            if(ypos > Screen.height) { ypos = Screen.height; }
            Vector3 curPos = new Vector3(xpos, ypos, zpos);
            //convert to worldpos.
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(curPos);
            transform.position = new Vector3(worldPos.x, 0.75f, worldPos.z);

            //find nearest spot
            _currentTargetDrop = null;
            GameObject closestTile = null;
            float distanceToNearest = float.MaxValue;
            foreach (GameObject tile in _theGameController.BlankTiles)
            {
                blankTileBehaviour blankTileScript = tile.GetComponent<blankTileBehaviour>();

                blankTileScript.ShowBorder(false);

                float distanceToTile = (transform.position - tile.transform.position).sqrMagnitude;
                if(distanceToTile < distanceToNearest)
                {
                    closestTile = tile;
                    distanceToNearest = distanceToTile;
                }
            }

            //highlight nearest spot (if it's not an illegal spot i.e. the same type)
            if (distanceToNearest < 1.5f)
            {
                blankTileBehaviour targetDropScript = closestTile.GetComponent<blankTileBehaviour>();
                if (targetDropScript.GetCurrentPlacedType() != GetTileType())
                {
                    Color colourToIndicate = GetHighLightColour(targetDropScript);
                    targetDropScript.ShowBorder(true, colourToIndicate);
                }
                _currentTargetDrop = closestTile;
            }
        }
    }

    Color GetHighLightColour(blankTileBehaviour placementTile)
    {
        Color colourToIndicate = GetSpecificHighLightColour(placementTile);
        colourToIndicate.a = 0.75f;
        colourToIndicate.r *= 0.75f;
        colourToIndicate.g *= 0.75f;
        colourToIndicate.b *= 0.75f;
        return colourToIndicate;
    }

    void OnMouseUp()
    {
        if (IsDraggable)
        {
            if (_currentTargetDrop != null)
            {
                blankTileBehaviour targetDropScript = _currentTargetDrop.GetComponent<blankTileBehaviour>();
                if (targetDropScript.GetCurrentPlacedType() != GetTileType())
                {
                    DropOnBlankTile(_currentTargetDrop);
                    _theGameController.TileDropped();
                }
                else
                {
                    //not allowed to place on same type - shunt aside to (hopefully) indicate this
                    transform.position = _tilePlacementPosition.transform.position;
                }
            }
        }
    }
    
    public void DropOnBlankTile(GameObject blankTile)
    {
        DropSoundAudio.clip = clips[Random.Range(0, clips.Length)];
        DropSoundAudio.Play();
        transform.position = blankTile.transform.position;
        IsDraggable = false;
        blankTileBehaviour blankTileScript = blankTile.GetComponent<blankTileBehaviour>();
        blankTileScript.ShowBorder(false);
        Destroy(blankTileScript.CurrentTile);
        blankTileScript.CurrentTile = gameObject;
        DoDropEffects(blankTileScript);
    }

    public void SetPhysics(bool active)
    {
        if(active)
        {
            IsDraggable = true;
            GetComponent<Rigidbody>().isKinematic = false;
        }
        else
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.rotation = Quaternion.Euler(new Vector3(270, 0, 0));
        }
    }

    public void TurnToWasteland(float secondsToTake)
    {
        _wastelandTransformationTime = secondsToTake;
        _wastelandTransformationStart = Time.time;
    }

    public void MarkWastelandEffect(int numWasteLandsAffecting)
    {
        float tintAmount;
        switch (GetTileType())
        {
            case eTileType.Farm:
                tintAmount = 0.6f;
                break;
            case eTileType.City:
                tintAmount = 0.15f;
                break;
            case eTileType.Water:
                tintAmount = 0.4f;
                break;
            default:
                tintAmount = 0.4f;
                break;
        }
        //tint red for every wasteland
        Color currentColor = new Color(_startingColor.r + (numWasteLandsAffecting* tintAmount), 
                                        _startingColor.g, 
                                        _startingColor.b);
        GetComponent<Renderer>().material.color = currentColor;
    }

    /////////////STUFF THAT SHOULD BE OVERRIDDEN BY CHILDREN//////////////////////////////////

    public virtual void Decorate()                                                              { }
    protected virtual string GetName()                                                          { return "TILE"; }
    public virtual Color GetSpecificHighLightColour(blankTileBehaviour placementTile = null)    { return Color.black; }
    protected virtual string GetInfo()                                                          { return "Tile Info"; }
    public virtual eTileType GetTileType()                                                      { return eTileType.Blank; }
    protected virtual void DoDropEffects(blankTileBehaviour tileIWasDroppedOn)                  { }
}
