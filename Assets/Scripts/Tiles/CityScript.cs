﻿using UnityEngine;
using System.Collections;

public class CityScript : TileBehaviour {

    public int Level
    {
        get
        {
            return _level;
        }
        set
        {
            _level = value;
            Decorate();
        }
    }
    int _level;

	// Use this for initialization
	void Start () {
        Level = 0;
        base.DoGeneralStart();
    }
	
	// Update is called once per frame
	void Update () {
        BaseUpdate();
    }

    float GetScaleFromLevel(int level)
    {
        float ret = level * 0.1f + 0.3f;
        ret = ret > 1.1f ? 1.1f : ret;
        return ret;
    }

    public override void Decorate()
    {
        //reset everything
        foreach (Transform child in transform)
        {
            //TODO why doesnt this work...sort out the rotation confusion
            //child.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            //child.transform.position = new Vector3(child.transform.position.x, child.transform.position.y, 0.2f);
            child.gameObject.SetActive(false);
        }

        //this should tend to cluster buildings towards the centre
        //TODO lots of hardcoded stuff here.
        int count = 0;
        while (count < Level)
        {
            foreach (Transform child in transform)
            {
                if (Random.Range(0.0f, 1.0f) > 0.6f)
                {
                    if (child.gameObject.activeSelf)
                    {
                        if (child.transform.localScale.y < 1.0f)
                        {
                            child.transform.localScale += new Vector3(0, 0.2f, 0);
                            child.transform.position += new Vector3(0, 0.1f, 0);
                            count++;
                        }
                    }
                    else
                    {
                        child.gameObject.SetActive(true);
                        count++;
                    }
                    break;
                }
            }
        }
    }

    protected override string GetName()
    {
        return "CITY";
    }

    protected override string GetInfo()
    {
        return
@"Cities generate 10 points multiplied by their level.

Level starts at 1, and increases every turn up to a max.

Max level is increased for every connected farm, and if the city touches water. 

If a city has 2 or more neighbours, it will attack and destroy any smaller neighbours.";
    }

    public override Color GetSpecificHighLightColour(blankTileBehaviour placementTile = null)
    {
        return Color.grey;
    }

    public override eTileType GetTileType()
    {
		//We do things this way to simplify the logic in GameControllerScript
		if(IsBusyTurningToWasteland())
		{
			return eTileType.WasteLand;
		}
		else
		{
			return eTileType.City;
		}
    }
}
