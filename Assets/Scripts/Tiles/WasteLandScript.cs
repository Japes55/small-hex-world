﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WasteLandScript : TileBehaviour
{

	// Use this for initialization
	void Start () {
        base.DoGeneralStart();
    }

    // Update is called once per frame
    void Update () {
        BaseUpdate();
    }

    public override void Decorate()
    {
        foreach (GameObject prop in Props)
        {
            bool visible = Random.Range(0.0f, 1.0f) > 0.5f;
            if (visible)
            {
                Vector2 pos = Random.insideUnitCircle * 0.7f;
                GameObject propInstance = Instantiate(prop, 
                                            transform.position + new Vector3(pos.x, 0.03f, pos.y), 
                                            Quaternion.Euler(new Vector3(0, Random.Range(0.0f, 360.0f), 0))) as GameObject;
                propInstance.transform.parent = transform;
            }
        }
    }

    protected override void DoDropEffects(blankTileBehaviour tileIWasDroppedOn)
    {
        //make all surrounding spaces indicate "-10%" temporarily
        foreach (blankTileBehaviour neighboursScript in tileIWasDroppedOn.NearestNeighbours)
        {
            neighboursScript.FloatMessage("-10%");
        }
    }

    protected override string GetName()
    {
        return "WASTELAND";
    }

    protected override string GetInfo()
    {
        return
@"Wastelands reduce your total points by 5.

Neighbouring tiles receive a 10% penalty to production.";
    }

    public override Color GetSpecificHighLightColour(blankTileBehaviour placementTile = null)
    {
        return Color.red;
    }

    public override eTileType GetTileType()
    {
        return eTileType.WasteLand;
    }
}
