﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicToggleHandler : MonoBehaviour {

    public Sprite MusicOnSprite;
    public Sprite MusicOffSprite;

    // Use this for initialization
    void Start () {
        SetSound(GameSettings.SoundOn);
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void ToggleMusic()
    {
        GameSettings.SoundOn = !GameSettings.SoundOn;
        SetSound(GameSettings.SoundOn);
    }

    void SetSound(bool on)
    {
        if (on)
        {
            AudioListener.volume = 1f;
            gameObject.GetComponent<Button>().image.sprite = MusicOnSprite;
        }
        else
        {
            AudioListener.volume = 0f;
            gameObject.GetComponent<Button>().image.sprite = MusicOffSprite;
        }
    }
}
