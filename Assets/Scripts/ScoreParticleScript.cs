﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreParticleScript : MonoBehaviour {

    public GameObject _targetPosition;
    public GameObject _theGameController;

    float _peakHeight; //the maximum height we want the particle to reach in its parabolic arc toward the target
    float _alpha; //the exponent of the parabolic arc for the given height and distance.
    Vector3 _startPosition;
    Vector3 _lineToTarget;
    float _totalDistance;
    float _speed;

    // Use this for initialization
    void Start () {
        //TODO not ideal that i call this on the creation of every particle - possible performance issues here.
        _targetPosition = GameObject.Find("Score Position");
        _theGameController = GameObject.Find("GameController");

        _speed = 5; //1 unit per second
        _peakHeight = 2.5f;
        transform.position = new Vector3(transform.position.x, _targetPosition.transform.position.y, transform.position.z);
        _startPosition = transform.position;
        _lineToTarget = _targetPosition.transform.position - _startPosition;
        _totalDistance = (_targetPosition.transform.position - _startPosition).magnitude;
        //get alpha - this is the "a" value of the quadratic equation determining the movement of the particle.
        _alpha = (4 * _peakHeight )/ (_totalDistance * _totalDistance);

    }

    //Our particle moves in a parabolic arc from start to end, reaching a max height of _peakHeight midway.
    //we consider the line from our starting point to our end point the "x" axis of the parabola.
    //the parabolas formula is: y = -_alpha(x - _totalDistance/2)^2 + _peakHeight
    void Update () {
        //update the position along the "x axis"
        Vector3 currentProjectedPositionAlongLine = Vector3.Project(transform.position - _startPosition, _lineToTarget);
        Vector3 desiredProjectedPositionAlongLine = currentProjectedPositionAlongLine + 
                                                    _lineToTarget.normalized*(_speed*Time.deltaTime);
        float distanceAlongPathThisFrame = (desiredProjectedPositionAlongLine).magnitude;
        //get the squared part
        float xSquared = (distanceAlongPathThisFrame - (_totalDistance / 2));
        xSquared = xSquared * xSquared;
        //plug into formula for parabola
        float heightThisFrame = -_alpha*xSquared + _peakHeight;
        //update position
        transform.position = _startPosition + new Vector3(desiredProjectedPositionAlongLine.x, 
            heightThisFrame, 
            desiredProjectedPositionAlongLine.z);

        //speed up over time
        _speed += Time.deltaTime*10;

        //die when we get to our destination (or overshoot)
        if (_totalDistance - distanceAlongPathThisFrame < 0.01f ||
            distanceAlongPathThisFrame > _totalDistance)
        {
            _theGameController.GetComponent<GameControllerScript>().ReportScoreParticleAnimationComplete();
            Destroy(this.gameObject);
        }
	}
}
