﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingMessageScript : MonoBehaviour {

    float _secondsToLive = 1.5f;
    float _distanceToFloat = 0.5f;
    TextMesh _text;
    float _startTime;
    Vector3 _startPosition;

    // Use this for initialization
    void Start () {
        _text = GetComponent<TextMesh>();
        _startTime = Time.time;
        _startPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        //float up gracefully and fade away
        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 
                                1 - (Time.time - _startTime) / _secondsToLive);
        transform.position = _startPosition + new Vector3(0,0,
                            _distanceToFloat * ((Time.time - _startTime) / _secondsToLive));
    }
}
