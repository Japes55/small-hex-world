﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;

public class HighScoresController : MonoBehaviour {

    private const string _server1 = "https://jpandaliciawedding.co.za";
    private const string _server2 = "https://japes.co.za";
    private string _webSiteAddr;
    public Text ServerIndicatorText;
    private const string _serverUrlFile = "HighScoreServerUrl.txt";
    private const string _secretKey = "saltyNonsense"; // Edit this value and make sure it's the same as the one stored on the server
    private const string _addScoreScriptURL = "/gamesDatabase/SmallHexWorld/addscore.php?"; //be sure to add a ? to your url
    private const string _getHighScoreScriptURL = "/gamesDatabase/SmallHexWorld/display.php";

    GameObject _gameOverScreen;
    GameObject _gameOverDialog;
    GameObject _highScoreScreen;

    float _score;

    InputField _nameInput;
    Text _highScoresNameText;
    Text _highScoresScoreText;
    Text _highScoresRankText;

    bool _respondToSubmitButton = false;

    // Use this for initialization
    void Start () {
        _gameOverScreen = GameObject.Find("GameOverScreen");
        _gameOverDialog = _gameOverScreen.transform.Find("GameOverDialog").gameObject;
        _nameInput = _gameOverDialog.transform.Find("InputField").GetComponent<InputField>();
        _gameOverScreen.SetActive(false);
        _highScoreScreen = GameObject.Find("HighScoreScreen");
        _highScoresNameText = GameObject.Find("NameText").GetComponent<Text>();
        _highScoresScoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        _highScoresRankText = GameObject.Find("RankText").GetComponent<Text>();
        _highScoreScreen.SetActive(false);
        _respondToSubmitButton = true;
        if (File.Exists(_serverUrlFile))
        {
            _webSiteAddr = File.ReadAllText(_serverUrlFile);
        }
        else
        {
            _webSiteAddr = _server1;
        }
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void ShowScoreController(float score)
    {
        _gameOverScreen.SetActive(true);
        _gameOverDialog.transform.Find("Heading").GetComponent<Text>().text = "Final Score\n" + score;
        _score = score;
    }

    public void SubmitScore()
    {
        if (_respondToSubmitButton)
        {
            _respondToSubmitButton = false;
            StartCoroutine(PostScores(_nameInput.text, (int)_score, true));
        }
    }

    //This does actually submit the score (sneaky sneaky!)
    public void DontSubmitScore()
    {
        if (_respondToSubmitButton)
        {
            _respondToSubmitButton = false;
            StartCoroutine(PostScores("<not submitted by user>", (int)_score, false));
        }
        else
        {
            //this could happen if someone's actually clicked submit already
            gameObject.SendMessage("RestartGame");
        }
    }

    // remember to use StartCoroutine when calling this function!
    IEnumerator PostScores(string name, int score, bool userSubmitted)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        int iUserSubmitted = (userSubmitted ? 1 : 0);
        string hash = GameObjectUtilities.Md5Sum(name + score + iUserSubmitted + _secretKey);

        string post_url = _webSiteAddr + _addScoreScriptURL + "name=" + WWW.EscapeURL(name) + 
                                                            "&score=" + score +
                                                            "&userSubmitted=" + iUserSubmitted +
                                                            "&hash=" + hash;

        Debug.Log("posting: " + post_url);
        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        if (userSubmitted)
        {
            //wait for response then submit high score
            yield return hs_post; // Wait until the download is done

            _respondToSubmitButton = true;

            if (hs_post.error != null)
            {
                print("There was an error posting the high score: " + hs_post.error);
            }
            else
            {
                ShowHighScores();
            }
        }
        else
        {
            //just assume that score went through, and restart game
            gameObject.SendMessage("RestartGame");
        }
    }

    public void ShowHighScores()
    {
        _highScoresRankText.text = "Fetching...";
        _highScoresNameText.text = "";
        _highScoresScoreText.text = "";
        _gameOverScreen.SetActive(false);
        _highScoreScreen.SetActive(true);
        StartCoroutine(GetScores());
    }

    public void HideHighScores()
    {
        _highScoreScreen.SetActive(false);
    }

    // remember to use StartCoroutine when calling this function!
    IEnumerator GetScores()
    {
        print("Doing a get: " + _webSiteAddr + _getHighScoreScriptURL);
        WWW hs_get = new WWW(_webSiteAddr + _getHighScoreScriptURL);
        yield return hs_get;

        if (hs_get.error != null)
        {
            print("There was an error getting the high score: " + hs_get.error);
        }
        else
        {
            _highScoresNameText.text = "";
            _highScoresScoreText.text = "";
            _highScoresRankText.text = "";
            //TODO should return JSON here

            print("Got this back: " + hs_get.text);

            HighScoreResultArray resultArray = JsonUtility.FromJson<HighScoreResultArray>(hs_get.text);
            int rank = 1;
            foreach (HighScoreResult result in resultArray.results)
            {
                _highScoresRankText.text += rank + "\n";
                _highScoresNameText.text += result.name + "\n";
                _highScoresScoreText.text += result.score.ToString() + "\n";
                rank++;
            }
        }
    }

    //This is a way to change score server after deployment.
    //To activate, click top right of screen while holding down q, t, p
    public void SwapServer()
    {
        if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.T) && Input.GetKey(KeyCode.P))
        {
            if (File.Exists(_serverUrlFile))
            {
                _webSiteAddr = File.ReadAllText(_serverUrlFile);
                if (_webSiteAddr == _server2)
                {
                    File.WriteAllText(_serverUrlFile, _server1);
                }
                else
                {
                    File.WriteAllText(_serverUrlFile, _server2);
                }
            }
            else
            {
                File.WriteAllText(_serverUrlFile, _server1);
            }

            _webSiteAddr = File.ReadAllText(_serverUrlFile);
            ServerIndicatorText.text = _webSiteAddr;
        }
    }
}

[Serializable]
public class HighScoreResultArray
{
    public HighScoreResult[] results;
}

[Serializable]
public class HighScoreResult
{
    public string name;
    public int score;
    public DateTime dateAdded;
}