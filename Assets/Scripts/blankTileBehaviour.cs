﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class blankTileBehaviour : MonoBehaviour {

    public GameObject CurrentTile;
    TileBehaviour _currentTileScript { get { return CurrentTile.GetComponent<TileBehaviour>(); } }
    public GameObject ScoreParticle;
    public GameObject FloatingMessage;

    public List<blankTileBehaviour> NearestNeighbours { get; private set; }
    public List<blankTileBehaviour> SecondNearestNeighbours { get; private set; }

    const float DISTANCE_BETWEEN_HEXES = 1.55f;

    public GameObject UIScoreDisplay;
    GameObject _uiScoreDisplay; //actually want to make a new one every time, with a kif fade effect
    public float Score { get; private set; }

    GameObject _borderIndicators;

    bool _showNormalScore = true;

    // Use this for initialization
    void Start () {
        //setup UI score display
        _uiScoreDisplay = Instantiate(UIScoreDisplay, transform.position,
                                Quaternion.identity) as GameObject;
        _uiScoreDisplay.transform.SetParent(transform);
        //lock the UI elements to a position in world space
        Camera mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Vector3 uiScoreDisplayWorldPos = transform.position;
        Vector3 uiScoreDisplayScreenPos = mainCamera.WorldToScreenPoint(uiScoreDisplayWorldPos);
        _uiScoreDisplay.transform.position = uiScoreDisplayScreenPos;
        GameObject mainCanvas = GameObject.Find("TileScoresParent");
        _uiScoreDisplay.transform.SetParent(mainCanvas.transform);
        _uiScoreDisplay.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        _borderIndicators = transform.Find("BorderIndicators").gameObject;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.AltGr) ||
            Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.LeftControl))
        {
            _uiScoreDisplay.SetActive(false);
        }
        else
        {
            _uiScoreDisplay.SetActive(true);
        }
    }

    //sets up this guys list of neighbours
    public void SetNeighbours(GameObject[] allBlankTiles)
    {
        NearestNeighbours = new List<blankTileBehaviour>();
        SecondNearestNeighbours = new List<blankTileBehaviour>();
        foreach (GameObject go in allBlankTiles)
        {
            if (go != this.gameObject)
            {
                if ((go.transform.position - transform.position).magnitude < (DISTANCE_BETWEEN_HEXES + 0.2f)) //tiles are 1.55 apart
                {
                    NearestNeighbours.Add(go.GetComponent<blankTileBehaviour>());
                }
                else if ((go.transform.position - transform.position).magnitude < (DISTANCE_BETWEEN_HEXES * 2 + 0.2f))
                {
                    SecondNearestNeighbours.Add(go.GetComponent<blankTileBehaviour>());
                }
            }
        }
    }

    public TileBehaviour.eTileType GetCurrentPlacedType()
    {
        if (CurrentTile == null)
        {
            return TileBehaviour.eTileType.Blank;
        }
        else
        {
            return _currentTileScript.GetTileType();
        }
    }

    //returns a list of all the spots that are connected to this spot that have the same type
    //tile placed on them.
    public List<blankTileBehaviour> GetContiguousTiles()
    {
        List<blankTileBehaviour> ret = new List<blankTileBehaviour>();
        AddContiguousTiles(ref ret);
        return ret;
    }

    //checks it's neighbours and adds any contiguous tiles that aren't already in the given list
    public void AddContiguousTiles(ref List<blankTileBehaviour> listSoFar)
    {
        if (!listSoFar.Contains(this))
        {
            listSoFar.Add(this);
            foreach (blankTileBehaviour neighboursScript in NearestNeighbours)
            {
                if (neighboursScript.GetCurrentPlacedType() == GetCurrentPlacedType())
                {
                    neighboursScript.AddContiguousTiles(ref listSoFar);
                }
            }
        }
    }

    //counts how many neighbouring tiles are of a given type.
    //closeness: 1 = nearest neighbours, 2 = 2nd nearest neighbours, etc
    public int GetNumNeighboursOfType(int neighbourCloseness, TileBehaviour.eTileType type)
    {
        int numFound = 0;
        if (neighbourCloseness >= 1)
        {
            foreach (blankTileBehaviour thisNeighboursScript in NearestNeighbours)
            {
                if (thisNeighboursScript.GetCurrentPlacedType() == type)
                {
                    numFound++;
                }
            }
        }
        if (neighbourCloseness >= 2)
        {
            foreach (blankTileBehaviour thisNeighboursScript in SecondNearestNeighbours)
            {
                if (thisNeighboursScript.GetCurrentPlacedType() == type)
                {
                    numFound++;
                }
            }
        }

        return numFound;
    }

    public List<blankTileBehaviour> GetNeighboursOfType(TileBehaviour.eTileType type)
    {
        List<blankTileBehaviour> ret = new List<blankTileBehaviour>();
        foreach (blankTileBehaviour thisNeighboursScript in NearestNeighbours)
        {
            if (thisNeighboursScript.GetCurrentPlacedType() == type)
            {
                ret.Add(thisNeighboursScript);
            }
        }
        return ret;
    }

    public void SetScore(float score)
    {
        Score = score;
        _uiScoreDisplay.GetComponent<Text>().text = score.ToString();
    }

    //creates pretty particles that fly up towards the total score
    public IEnumerator SpawnScoreParticles(int particleSize)
    {
        //make score particles
        //for (int i = 0; i < numParticles; i++)
        //{
            Vector2 pos = Random.insideUnitCircle * 0.7f;
            GameObject particle = Instantiate(ScoreParticle, 
                                transform.position + new Vector3(pos.x, 0.03f, pos.y),
                                Quaternion.Euler(new Vector3(0, 0, 90))) as GameObject;

        //get colour - doesn't work currently because the particle's material's shader doesn't have a colour.
        particle.GetComponent<Renderer>().material.color = _currentTileScript.GetSpecificHighLightColour();
        if (particleSize > 1)
        {
            particle.transform.localScale *= particleSize / 2;
        }
            yield return new WaitForSeconds(0.5f);
        //}
    }

    //makes its border indicators visible at a certain colour
    public void ShowBorder(bool show, Color? colour = null)
    {
        _borderIndicators.SetActive(show);
        Color colourToSet = Color.black;
        if (colour.HasValue)
        {
            colourToSet = colour.Value;
        }
        foreach(var childRenderer in _borderIndicators.transform.GetComponentsInChildren<Renderer>())
        {
            childRenderer.material.color = colourToSet;
        }
    }

    //make some text float up from the tile and then fade away
    public void FloatMessage(string msg)
    {
        Debug.Log("Instantiating message at " + transform.position + new Vector3(0.0f, 0.1f, 0.0f));
        GameObject floatmsg = Instantiate(FloatingMessage, transform.position + new Vector3(0.1f, 0.5f, 0.1f),
                                Quaternion.Euler(new Vector3(90, 0, 0))) as GameObject;
        floatmsg.GetComponent<TextMesh>().text = msg;
    }
}
